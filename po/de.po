# English translations for Flare package.
# Copyright (C) 2022 THE Flare'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Flare package.
# Julian Schmidhuber <translation@schmiddi.anonaddy.com>, 2022.
# Jürgen Benvenuti <gastornis@posteo.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-20 16:48+0200\n"
"PO-Revision-Date: 2023-05-29 08:43+0000\n"
"Last-Translator: Julian Schmidhuber <gitlab@schmiddi.anonaddy.com>\n"
"Language-Team: German <https://hosted.weblate.org/projects/flare/flare/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18-dev\n"

#: data/resources/ui/about.ui:14
msgid "translator-credits"
msgstr ""
"Julian Schmidhuber <translation@schmiddi.anonaddy.com>\n"
"Jürgen Benvenuti <gastornis@posteo.org>, 2022\n"
"Leo Moser <leomoser99@gmail.com>, 2023"

#: data/resources/ui/attachment.ui:26
msgid "Load image"
msgstr "Bild laden"

#: data/resources/ui/attachment.ui:37
msgid "Load video"
msgstr "Video laden"

#: data/resources/ui/attachment.ui:48
msgid "Load file"
msgstr "Datei laden"

#: data/resources/ui/attachment.ui:131
msgctxt "accessibility"
msgid "Open with…"
msgstr "Öffnen mit…"

#: data/resources/ui/attachment.ui:147
msgctxt "accessibility"
msgid "Download"
msgstr "Herunterladen"

#: data/resources/ui/channel_info_dialog.ui:7 data/resources/ui/window.ui:172
msgid "Channel Information"
msgstr "Kanalinformationen"

#: data/resources/ui/channel_info_dialog.ui:33
msgctxt "accessibility"
msgid "Reset Session"
msgstr "Setze Verbindung zurück"

#: data/resources/ui/channel_info_dialog.ui:35
msgid "Reset Session"
msgstr "Setze Verbindung zurück"

#: data/resources/ui/channel_info_dialog.ui:51
#: data/resources/ui/error_dialog.ui:14
msgid "Close"
msgstr "Schließen"

#: data/resources/ui/channel_list.ui:15
#: data/resources/ui/channel_messages.ui:17
msgid "Offline"
msgstr "Offline"

#: data/resources/ui/channel_list.ui:31
msgid "Search"
msgstr "Suchen"

#: data/resources/ui/channel_list.ui:33 data/resources/ui/window.ui:58
msgctxt "accessibility"
msgid "Search"
msgstr "Suchen"

#: data/resources/ui/channel_messages.ui:31
msgid "No Channel Selected"
msgstr "Kein Kanal ausgewählt"

#: data/resources/ui/channel_messages.ui:32
msgid "Select a channel"
msgstr "Einen Kanal auswählen"

#: data/resources/ui/channel_messages.ui:139
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "Antwort entfernen"

#: data/resources/ui/channel_messages.ui:141
msgctxt "tooltip"
msgid "Remove reply"
msgstr "Antwort entfernen"

#: data/resources/ui/channel_messages.ui:171
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "Anhang entfernen"

#: data/resources/ui/channel_messages.ui:173
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "Anhang entfernen"

#: data/resources/ui/channel_messages.ui:207
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "Anhang hinzufügen"

#: data/resources/ui/channel_messages.ui:209
msgctxt "tooltip"
msgid "Add attachment"
msgstr "Anhang hinzufügen"

#: data/resources/ui/channel_messages.ui:228
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr "Füge ein Emoji ein"

#: data/resources/ui/channel_messages.ui:230
msgctxt "tooltip"
msgid "Insert emoji"
msgstr "Füge ein Emoji ein"

#: data/resources/ui/channel_messages.ui:238
msgctxt "accessibility"
msgid "Message input"
msgstr "Nachricht eingeben"

#: data/resources/ui/channel_messages.ui:243
msgctxt "tooltip"
msgid "Message input"
msgstr "Nachricht eingeben"

#: data/resources/ui/channel_messages.ui:249
msgctxt "accessibility"
msgid "Send"
msgstr "Senden"

#: data/resources/ui/channel_messages.ui:251
msgctxt "tooltip"
msgid "Send"
msgstr "Senden"

#: data/resources/ui/channel_messages.ui:252
msgid "Send"
msgstr "Senden"

#: data/resources/ui/dialog_clear_messages.ui:4 data/resources/ui/window.ui:156
msgid "Clear messages"
msgstr "Nachrichten löschen"

#: data/resources/ui/dialog_clear_messages.ui:5
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"Dies wird alle auf dem Gerät vorhandenen Nachrichten löschen und die "
"Anwendung schließen."

#: data/resources/ui/dialog_clear_messages.ui:9
#: data/resources/ui/dialog_unlink.ui:9 data/resources/ui/link_window.ui:16
msgid "Cancel"
msgstr "Abbrechen"

#: data/resources/ui/dialog_clear_messages.ui:10
msgid "Clear"
msgstr "Löschen"

#: data/resources/ui/dialog_unlink.ui:4 data/resources/ui/window.ui:152
msgid "Unlink"
msgstr "Entkoppeln"

#: data/resources/ui/dialog_unlink.ui:5
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"Dies wird dieses Gerät entkoppeln und alle lokal gespeicherten Daten "
"löschen. Dies wird außerdem die Anwendung schließen, sodass es nach dem "
"erneuten Öffnen neu gekoppelt werden kann."

#: data/resources/ui/dialog_unlink.ui:10
msgid "Unlink and delete all messages"
msgstr "Entkoppeln und alle Nachrichten löschen"

#: data/resources/ui/dialog_unlink.ui:11
msgid "Unlink but keep messages"
msgstr "Entkoppeln, aber Nachrichten behalten"

#: data/resources/ui/error_dialog.ui:8
msgid "Error"
msgstr "Fehler"

#: data/resources/ui/error_dialog.ui:59
msgid "Please consider reporting this error."
msgstr "Bitte erwäge, diesen Fehler zu melden."

#: data/resources/ui/link_window.ui:8
msgid "Link Device"
msgstr "Gerät koppeln"

#: data/resources/ui/link_window.ui:25
msgid "Please scan this QR code with your primary device."
msgstr "Bitte scanne diesen QR-Code mit deinem Hauptgerät."

#: data/resources/ui/link_window.ui:35
msgctxt "accessibility"
msgid "Copy to clipboard"
msgstr "In die Zwischenablage kopieren"

#: data/resources/ui/link_window.ui:47
msgid "Please wait until the contacts are displayed after linking."
msgstr "Bitte warte nach dem Koppeln, bis deine Kontakte angezeigt werden."

#: data/resources/ui/message_item.ui:248
msgid "Reply"
msgstr "Antworten"

#: data/resources/ui/message_item.ui:254
msgid "React"
msgstr "Reagieren"

#: data/resources/ui/message_item.ui:260
msgid "Delete"
msgstr "Löschen"

#: data/resources/ui/message_item.ui:267
msgid "Copy"
msgstr "Kopieren"

#: data/resources/ui/preferences_window.ui:9
msgid "General"
msgstr "Allgemein"

#: data/resources/ui/preferences_window.ui:13
msgid "Linking"
msgstr "Koppelung"

#: data/resources/ui/preferences_window.ui:14
msgid "The device will need to be relinked to take effect."
msgstr ""
"Das Gerät muss neu gekoppelt werden, damit diese Einstellung wirksam wird."

#: data/resources/ui/preferences_window.ui:17
msgid "Device Name"
msgstr "Name des Gerätes"

#: data/resources/ui/preferences_window.ui:25
msgid "Attachments"
msgstr "Anhänge"

#: data/resources/ui/preferences_window.ui:28
msgid "Dowload images automatically"
msgstr "Bilder automatisch herunterladen"

#: data/resources/ui/preferences_window.ui:42
msgid "Dowload videos automatically"
msgstr "Videos automatisch herunterladen"

#: data/resources/ui/preferences_window.ui:56
msgid "Dowload files automatically"
msgstr "Dateien automatisch herunterladen"

#: data/resources/ui/preferences_window.ui:73
msgid "Notifications"
msgstr "Benachrichtigungen"

#: data/resources/ui/preferences_window.ui:74
msgid "Notifications for new messages."
msgstr "Benachrichtigungen für neue Nachrichten."

#: data/resources/ui/preferences_window.ui:77
#: data/de.schmidhuberj.Flare.gschema.xml:37
msgid "Send notifications"
msgstr "Benachrichtigungen senden"

#: data/resources/ui/preferences_window.ui:91 src/gui/preferences_window.rs:29
msgid "Watch for new messages while closed"
msgstr "Empfange Benachrichtigungen im Hintergrund"

#: data/resources/ui/preferences_window.ui:109
msgid "Other"
msgstr "Andere"

#: data/resources/ui/preferences_window.ui:112
msgid "Selectable Message Text"
msgstr "Auswählbarer Nachrichtentext"

#: data/resources/ui/preferences_window.ui:113
msgid "Selectable text can interfere with swipe-gestures on touch-screens."
msgstr ""
"Auswählbarer Text kann mit Wischgesten auf Touchscreens Probleme bereiten."

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Tastaturkürzel anzeigen"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Go to settings"
msgstr "Zu den Einstellungen springen"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Go to about page"
msgstr "Zu Info springen"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Menü öffnen"

#: data/resources/ui/shortcuts.ui:38
msgctxt "shortcut window"
msgid "Close window"
msgstr "Fenster Schließen"

#: data/resources/ui/shortcuts.ui:46
msgctxt "shortcut window"
msgid "Channels"
msgstr "Kanäle"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr "Zu Kanal 1…9 springen"

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Search in channels"
msgstr "In den Kanälen suchen"

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr "Anhang hochladen"

#: data/resources/ui/shortcuts.ui:67
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr "Die Texteingabe fokussieren"

#: data/resources/ui/shortcuts.ui:73
msgctxt "shortcut window"
msgid "Load more messages"
msgstr "Weitere Nachrichten laden"

#: data/resources/ui/window.ui:36
msgctxt "accessibility"
msgid "Add Conversation"
msgstr "Unterhaltung Hinzufügen"

#: data/resources/ui/window.ui:46 data/resources/ui/window.ui:117
msgctxt "accessibility"
msgid "Menu"
msgstr "Menü"

#: data/resources/ui/window.ui:148
msgid "Settings"
msgstr "Einstellungen"

#: data/resources/ui/window.ui:160
msgid "Keybindings"
msgstr "Tastaturkürzel"

#: data/resources/ui/window.ui:164
msgid "About"
msgstr "Info"

#: src/backend/channel.rs:505
msgid "Note to self"
msgstr "Notiz an mich"

#: src/backend/manager.rs:378
msgid "You"
msgstr "Du"

#: src/backend/message/call_message.rs:115
msgid "Started calling."
msgstr "Anruf wurde begonnen."

#: src/backend/message/call_message.rs:116
msgid "Answered a call."
msgstr "Ein Anruf wurde beantwortet."

#: src/backend/message/call_message.rs:117
msgid "Hung up."
msgstr "Aufgelegt."

#: src/backend/message/call_message.rs:118
msgid "Is busy."
msgstr "Besetzt."

#: src/backend/message/text_message.rs:294
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] "Hat ein Bild gesendet"
msgstr[1] "Hat {} Bilder gesendet"

#: src/backend/message/text_message.rs:297
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] "Hat ein Video gesendet"
msgstr[1] "Hat {} Videos gesendet"

#: src/backend/message/text_message.rs:300
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] "Hat eine Datei gesendet"
msgstr[1] "Hat {} Dateien gesendet"

#: src/error.rs:87
msgid "I/O Error."
msgstr "Ein-/Ausgabefehler."

#: src/error.rs:92
msgid "There does not seem to be a connection to the internet available."
msgstr "Es scheint keine Internetverbindung vorhanden zu sein."

#: src/error.rs:97
msgid "Something glib-related failed."
msgstr "Etwas glib-bezogenes ist fehlgeschlagen."

#: src/error.rs:102
msgid "The communication with Libsecret failed."
msgstr "Die Kommunikation mit Libsecret ist fehlgeschlagen."

#: src/error.rs:107
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""
"In der Datenbank ist ein Fehler aufgetreten. Bitte starte die Anwendung neu "
"oder lösche die Datenbank und kopple die Anwendung erneut."

#: src/error.rs:112
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""
"Du scheinst nicht von Signal autorisiert zu sein. Bitte lösche die Datenbank "
"und kopple die Anwendung erneut."

#: src/error.rs:117
msgid "Sending a message failed."
msgstr "Nachricht konnte nicht gesendet werden."

#: src/error.rs:122
msgid "Receiving a message failed."
msgstr "Nachricht konnte nicht empfangen werden."

#: src/error.rs:127
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""
"Etwas Unerwartetes ist mit Signal passiert. Bitte versuche es später wieder."

#: src/error.rs:132
msgid "The application seems to be misconfigured."
msgstr "Die Anwendung scheint falsch konfiguriert zu sein."

#: src/error.rs:137
msgid "A part of the application crashed."
msgstr "Ein Teil der Anwendung ist abgestürzt."

#: src/error.rs:147
msgid "Please check your internet connection."
msgstr "Bitte prüfe deine Internetverbindung."

#: src/error.rs:152
msgid "Please delete the database and relink the device."
msgstr "Bitte lösche die Datenbank und kopple das Gerät erneut."

#: src/error.rs:159
msgid "The database path at {} is no folder."
msgstr "Der Datenbankpfad bei {} ist kein Ordner."

#: src/error.rs:164
msgid "Please restart the application with logging and report this issue."
msgstr ""
"Bitte starte die Anwendung neu mit Protokollierung und melde dieses Problem."

#: src/gui/preferences_window.rs:51
msgid "Background permission"
msgstr "Hintergrundberechtigung"

#: src/gui/preferences_window.rs:52
msgid "Use settings to remove permissions"
msgstr "Nutze die Einstellungen um diese Berechtigung zu entfernen"

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:57
msgid "%H:%M"
msgstr "%H:%M"

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:60
msgid "%Y-%m-%d %H:%M"
msgstr "%d.%m.%Y %H:%M"

#. Translators: Flare name in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml:8
msgid "Flare"
msgstr "Flare"

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:10
msgid "An unofficial Signal GTK client"
msgstr "Ein inoffizieller Signal GTK-Client"

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:26
msgid ""
"An unofficial GTK client for the messaging application Signal. This is a "
"very simple application with many features missing compared to the official "
"applications."
msgstr ""
"Ein inoffizieller GTK-Client für die Messaging-Anwendung Signal. Es handelt "
"sich um eine sehr einfache Anwendung mit vielen fehlenden Funktionen im "
"Vergleich zu den offiziellen Anwendungen."

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:32
msgid "Link device"
msgstr "Gerät koppeln"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:36
msgid "Send messages"
msgstr "Nachrichten senden"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:40
msgid "Receive messages"
msgstr "Nachrichten empfangen"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:44
msgid "Reply to a message"
msgstr "Auf eine Nachricht antworten"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:48
msgid "React to a message"
msgstr "Auf eine Nachricht reagieren"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:52
msgid "Sending and receiving attachments"
msgstr "Senden und Empfangen von Anhängen"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:56
msgid "Encrypted storage"
msgstr "Verschlüsselte Datenspeicherung"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:60
msgid "Notifications, optionally in the background"
msgstr "Benachrichtigungen, optional im Hintergrund"

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:65
msgid ""
"More features are planned, blocked or not-planned. Consult the README for "
"more information about them."
msgstr ""
"Weitere Funktionen geplant, blockiert oder nicht geplant. Beachte die README-"
"Datei für weitere Informationen über diese Funktionen."

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:69
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""
"Bitte beachten Sie, dass die Verwendung dieser Anwendung Ihre Sicherheit im "
"Vergleich zur Verwendung offizieller Signal-Anwendungen wahrscheinlich "
"verschlechtert. Seien Sie vorsichtig im Umgang mit sensiblen Daten. In der "
"README des Projekts finden Sie weitere Informationen zur Sicherheit."

#: data/de.schmidhuberj.Flare.metainfo.xml:397
msgid "Overview of the application"
msgstr "Übersicht der Anwendung"

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr "Fensterbreite"

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr "Fensterhöhe"

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr "Maximierter Zustand des Fensters"

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""
"Der Gerätename beim Verknüpfen von Flare. Dies erfordert, dass die Anwendung "
"neu gekoppelt wird."

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr "Bilder automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr "Videos automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download files"
msgstr "Dateien automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Run in background when closed"
msgstr "Nach dem Schließen im Hintergrund ausführen"

#: data/de.schmidhuberj.Flare.gschema.xml:46
msgid "Can messages be selected"
msgstr "Nachrichten können ausgewählt werden"

#~ msgctxt "accessibility"
#~ msgid "Reset Identity"
#~ msgstr "Identität zurücksetzen"

#~ msgid "Reset Identity"
#~ msgstr "Identität zurücksetzen"

#~ msgid "Load more"
#~ msgstr "Weitere laden"

#~ msgid "Message Storage"
#~ msgstr "Nachrichten-Speicher"

#~ msgid "Number of messages to load on startup"
#~ msgstr "Anzahl der Nachrichten, welche bei Anwendungsstart geladen werden"

#~ msgid "Per channel"
#~ msgstr "Pro Kanal"

#~ msgid "Number of messages to load on request"
#~ msgstr "Anzahl der Nachrichten, welche auf Anfrage geladen werden"

#~ msgid "The amount of messages to load on startup for each channel"
#~ msgstr ""
#~ "Anzahl der Nachrichten, welche bei Anwendungsstart für jeden Kanal "
#~ "geladen werden"

#~ msgid "The amount of messages to load on request for each channel"
#~ msgstr ""
#~ "Anzahl der Nachrichten, welche auf Anfrage für jeden Kanal geladen werden"

#, fuzzy
#~ msgid "Changed"
#~ msgstr "Kanäle"

#, fuzzy
#~ msgid "Unlink without deleting messages."
#~ msgstr "Entkoppeln, aber Nachrichten behalten"

#, fuzzy
#~ msgid "Messages and notifications for calls."
#~ msgstr "Benachrichtigungen senden"

#, fuzzy
#~ msgid "Greatly refactored messages."
#~ msgstr "Nachrichten löschen"

#, fuzzy
#~ msgid "Changed:"
#~ msgstr "Kanäle"

#, fuzzy
#~ msgid "Notification support."
#~ msgstr "Benachrichtigungen"

#, fuzzy
#~ msgid "New message storage backend."
#~ msgstr "Nachrichten-Speicher"

#, fuzzy
#~ msgid "Search for the channel view."
#~ msgstr "In den Kanälen suchen"

#, fuzzy
#~ msgid "Message storage."
#~ msgstr "Nachrichten-Speicher"

#, fuzzy
#~ msgid "Keyboard shortcuts."
#~ msgstr "Tastaturkürzel anzeigen"

#~ msgid ""
#~ "Notifications for new messages. These notifications will only work while "
#~ "Flare is open, if you also want to be notified when this application is "
#~ "closed, consider using something like messenger-notify."
#~ msgstr ""
#~ "Benachrichtigungen für neue Nachrichten. Diese Benachrichtigungen "
#~ "funktionieren nur, wenn Flare geöffnet ist. Wenn du auch benachrichtigt "
#~ "werden willst, wenn das Programm geschlossen ist, nutze beispielsweise "
#~ "messenger-notify."

#~ msgid "Are you sure?"
#~ msgstr "Bist du sicher?"

#~ msgid "For each channel"
#~ msgstr "Für jeden Kanal"
